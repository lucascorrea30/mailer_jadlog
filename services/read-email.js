var reqApiJadlog = require('./request-api-jadlog');
// const throttledQueue = require('throttled-queue')

// let throttle = throttledQueue(20, 1000) // 20 times per second
var logger = require('../logger').createLogger('ReadMail');

const { MongoClient } = require('mongodb');
var dbo;
var url = "mongodb://localhost:27017/jadlog_mailer";

require('events').EventEmitter.defaultMaxListeners = 20;

MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    dbo = db.db("jadlog_mailer");
});

var Imap = require("imap"),
    inspect = require('util').inspect;
var MailParser = require("mailparser").MailParser;
// var Promise = require("bluebird");
// Promise.longStackTraces();

const simpleParser = require('mailparser').simpleParser;

module.exports = class ReadEmail {
    constructor(user, password, host, port, tls, secure) {
        // process.setMaxListeners(100);
        this.imap = new Imap({
            user: user,
            password: password,
            host: host,
            port: port,
            tls: tls,
            secure: secure,
            agent: false,
            keepalive: { forceNoop: true }
        });
        // Promise.promisifyAll(this.imap);
        logger.info("Imap Criado");
    }

    read() {
        let imap = this.imap;
        imap.connect();

        imap.once("ready", function() {
            logger.info("Imap Ready");
            this.openBox("INBOX", false, function(err, mailBox) {
                if (err) {
                    logger.error(err);
                    console.error(err);
                } else {
                    logger.info("Open box: " + mailBox.name);
                    // console.log("User: "+user);
                    // console.log(mailBox);
                    imap.search(["UNSEEN", ["SINCE", "May 01, 2020"],
                        ["SUBJECT", "OCORRENCIA NA REMESSA "]
                    ], function(err, results) {
                        // imap.search([["SINCE", "May 01, 2020"], ["SUBJECT", "OCORRENCIA NA REMESSA "]], function(err, results) {
                        if (!results || !results.length || results.length <= 0) {
                            logger.info("Sem e-mails nao lidos");
                            console.log("No unread mails");
                            imap.end();
                            imap = null;
                            return;
                        } else if (err) {
                            logger.error("IMAP Search Error: ");
                            logger.error(err);
                            console.error("IMAP Search Error: ");
                            console.error(err);
                            imap.end();
                            return;
                        } else {
                            // var f = imap.seq.fetch('1:1', { bodies: "" });
                            // bodies: 'HEADER.FIELDS (FROM TO SUBJECT DATE)',
                            //var f = imap.seq.fetch('1:20', {bodies: ''});
                            var f = imap.fetch(results.slice(0, 50), { bodies: '' });
                            f.on("message", processMessage);
                            f.once("error", function(err) {
                                logger.error("Fatch Error: ");
                                logger.error(err);
                                console.error("Fatch Error: ");
                                console.error(err);
                                return reject(err);
                            });
                            f.once("end", function() {
                                logger.info("Fim de leitura dos e-mails");
                                console.log("Done fetching all unseen messages.");
                                console.log(imap._config.user);

                                /* mark as seen */
                                imap.setFlags(results.slice(0, 50), ['\\Seen'], function(err) {
                                    if (!err) {
                                        logger.info("E-mails marcados como lidos");
                                        console.log("marked as read");
                                    } else {
                                        logger.error("SetFlags Error");
                                        logger.error(err);
                                        console.error("SetFlags Error");
                                        console.log(JSON.stringify(err, null, 2));
                                    }
                                });
                                imap.end();
                                return;
                            });
                        }
                    });
                }
            });
        });
        imap.on("error", function(err) {
            logger.error("Connection error");
            logger.error(err.stack);
            console.log("Connection error: " + err.stack);
            if (imap != null) {
				imap.end();
			}
            return;
        });
        imap.on('timeout', function() {
            logger.warn('Caught uncaught timeout');
            console.log('Caught uncaught timeout');
            // imap.end();
        });

        imap.on("end", function() {
            logger.info("Imap END.");
            imap.end();
            imap = null;
            return;
        });

        // FUNCTIONS

        function processMessage(msg, seqno) {

            var parser = new MailParser();
            parser.on("headers", function(headers) {
                // console.log("Header: " + JSON.stringify(headers));
            });

            parser.on('data', data => {
                // console.log("MSG: "+seqno);
                // console.log("type: "+ data.type);
                // console.log("text: "+ data.text);
                // console.log("html: "+ data.html);

                // if (data.type === 'attachment') {
                //     console.log(data.filename);
                // data.content.pipe(process.stdout);
                // data.content.on('end', () => data.release());
                // }
            });

            msg.on("body", function(stream) {

                simpleParser(stream, (err, mail) => async function() {

                    var arr = showMessage(mail.html);

                    logger.info("Buscando clientes");
                    let resp = await getClient(email.remessa);

                    if (resp.status && resp.status == true && resp.client != null && resp.tracking != null) {
                        let texto = {
                            "remessa": arr[2],
                            "local": arr[4],
                            "motivo": arr[6],
                            "medida": arr[8]
                        }
                        let email = {
                            "data": resp.tracking.dtEmissao,
                            "dataEvento": trataData(mail.date),
                            "remessa": arr[2],
                            "texto": texto,
                            "status_remessa": resp.tracking.status,
                            "client": resp.client,
                            "sended": false,
                            "status": "",
                            "nfe": "",
                            "unidade": "",
                            "dest": "",
                            "cepDest": "",
                            "prazo": "",
                            "volumes": { "qnt": 1, "isUnity": false, "isSended": false },
                            "sendClient": false,
                            "sendedUnity": false
                        }
                        dbo.collection("emails").insertOne(email, function(err, res) {
                            //console.log(email);
                            if (err) {
                                logger.error(err);
                                console.error(err);
                            } else {
                                logger.info("1 Remessa inserida");
                                console.log(email.remessa + " - 1 email inserted");
                            }
                        });
                    }

                });

            });
            msg.once("end", function() {
                logger.info('msg end');
                // console.log("Finished msg #" + seqno);
                parser.end();
            });
        }

        function showMessage(html) {

            var arr = [];
            const htmlparser2 = require("htmlparser2");
            const parser = new htmlparser2.Parser({
                onopentag(name, attribs) {
                    // if (name === "strong") {
                    //     console.log("\nCAMPO");
                    // }
                    // if (name === "label") {
                    //     console.log("\nTEXTO");
                    // }
                },
                ontext(text) {
                    if (text.trim().length > 0) {
                        // console.log("-->", text);
                        arr.push(text.trim());
                    }
                },
                onclosetag(tagname) {
                    // if (tagname === "script") {
                    //     console.log("That's it?!");
                    // }
                }
            }, { decodeEntities: true });
            parser.write(
                html
            );
            parser.end();
            // console.log(arr);

            return arr;
        }

        async function getClient(remessa) {
            var answer = { status: false, client: null, tracking: null };

            let clients = await dbo.collection("clients").find({}).toArray();
            if (clients != null && clients !== undefined) {
                for (var client of clients) {

                    let response = await getTracking(remessa, client.token);
                    if (response !== undefined && response != null && response.consulta !== undefined) {
                        var resp = response.consulta[0];
                        if (resp.error) {
                            // console.log("Cliente "+client.name+":");
                            // console.log(resp.error);
                            // loggerMain.error(resp.error);
                        } else {
                            answer.status = true;
                            answer.client = client;
                            answer.tracking = resp.tracking;

                            return answer;
                        }
                    }

                }
            }

            return answer;

        }

        async function getXml(dacte, token, callback) {
            var data = { "dacte": dacte };
            var path = "/cte/xml";
            var type = "application/xml";

            callback(await reqApiJadlog(data, path, token, type));
        }

        async function getTracking(cte, token, callback) {
            var data = { "consulta": [{ "cte": cte }] };
            var path = "/tracking/consultar";
            var type = "application/json";

            callback(await reqApiJadlog(data, path, token, type));
        }

        function trataData(data) {
            if (data !== undefined && data.toString().length > 1) {
                const mount = {
                    'Jan': 1,
                    'Feb': 2,
                    'Mar': 3,
                    'Apr': 4,
                    'May': 5,
                    'Jun': 6,
                    'Jul': 7,
                    'Aug': 8,
                    'Sep': 9,
                    'Oct': 10,
                    'Nov': 11,
                    'Dec': 12
                }
                var aux = data.toString().split(' ');
                return aux[2] + '/' + mount[aux[1]] + '/' + aux[3];
            } else {
                return data;
            }
        }

    }

}