var nodemailer = require('nodemailer');
var logger = require('../logger').createLogger('SendMail');

module.exports = class SendMail {
    constructor(user, password, host, port, secure, tls) {
        this.remetente = nodemailer.createTransport({
            pool: true,
            host: host,
            port: port,
            secure: secure,
			tls: tls,
            auth: {
                user: user,
                pass: password
            }
        });
    }

    send(from, to, subject, msg, attach, callback) {
        logger.info("Created");
        var emailASerEnviado = {
            from: from,
            to: to,
            subject: subject,
            html: msg,
        };
        if (attach != null) {
            logger.info("has Attach");
            emailASerEnviado.attachments = [attach];
        }
		logger.info("E-mail: ");
		logger.info(emailASerEnviado);

        this.remetente.sendMail(emailASerEnviado, function(error){
            if (error) {
                logger.error(error);
                console.log(error);
                return false;
            } else {
                logger.info('Email enviado com sucesso: '+emailASerEnviado);
                console.log('Email enviado com sucesso: '+emailASerEnviado);
                callback();
            }
        });
        // if (this.remetente) this.remetente.close();
    }
    
}
