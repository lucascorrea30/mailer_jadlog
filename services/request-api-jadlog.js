const rp = require('request-promise');
var data = { "dacte": "32200504884082000569570000024634031024634038" };
var path = "/cte/xml";
var path = "/tracking/consultar";
var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOjgyMjA4LCJkdCI6IjIwMjAwNDE1In0.5eu8WIjgxlbmIX9tPVXyHc8A72FTp5p8f134S_azDUo";
// var logger = require('../logger').createLogger('RequestAPI');

module.exports = async function getInfoJadLog(data, path, token, type) {

    var options = {
        method: 'POST',
        body: data,
        json: true,
        url: "http://www.jadlog.com.br/embarcador/api" + path,
        headers: {
            'accept': type,
            'content-type': 'application/json',
            'Authorization': "Bearer " + token
        }
    };
    // logger.info("Created");
    // logger.info(options);

    // call the request
    return rp(options).then(function(body) {
            return body;
        })
        .catch(function(err) {
            return err;
        });

}