// var reqApiJadlog = require('./request-api-jadlog');
// const SendEmail = require('./send-email');
// const reqSendEmail = new SendEmail('all.crc@jadlog.com.br', 'Trocar123@', 'email-ssl.com.br', 465, 'email-ssl.com.br', true);

// const {MongoClient, ObjectId} = require('mongodb');
// var dbo;
// var url = "mongodb://localhost:27017/jadlog_mailer";

// var moment = require('moment-business-days');

var logger = require('../logger').createLogger('test'); // logs to STDOUT

logger.info("teste info");
logger.error("teste error");
logger.warn("teste warn");
logger.fatal(__dirname + '\\..\\');

console.log(__dirname);

// moment.updateLocale('pt-BR', {
//     workingWeekdays: [1, 2, 3, 4, 5]
//  });

// MongoClient.connect(url, function(err, db) {
//     if (err) throw err;
//     dbo = db.db("jadlog_mailer");

//     const {ObjectId} = require('mongodb'); // or ObjectID 
    
//   dbo.collection("emails").find({"remessa":{$in:r}}).toArray(function(err, emails) {
    
//     let arrFinal = [];
// 	let count = 0;
// 	emails.forEach(email => {
// 		count++;
// 		arrFinal.push(email.remessa);
// 	});
// 	console.log(arrFinal);
// 	console.log("Count: "+count);
//   }); // dbo.collection("attendants")
  
    
// });

function trataData(data) {
    if (data !== undefined && data.length > 1) {
        return data.substring(8,10) +'/'+ data.substring(5,7) +'/'+ data.substring(0,4);
    } else {
        return data;
    }
  }

  function getTracking(cte, token, callback) {
    var data = {"consulta":[{"cte":cte}]};
    var path = "/tracking/consultar";
    var type = "application/json";
    
    reqApiJadlog(data, path, token, type, function(err, res, body){
      if (err) {
        console.error(err);
      } else {
        if (body == undefined) console.error("Retorno getTracking undefined");
        else callback(body);
      }
    });
  }
  
  function getXml(dacte, token, callback) {
    var data = {"dacte": dacte};
    var path = "/cte/xml";
    var type = "application/xml";
      
    reqApiJadlog(data, path, token, type, function(err, res, body){
      if (err) {
        console.error(err);
      } else {
        if (body == undefined) console.error("Retorno getXml undefined");
        else callback(body);
      }
    });
  }
  