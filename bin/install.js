var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'Jadlog Mailer',
  description: 'Sistema automatizador de emails da Jadlog. Feito por Lucas Correa',
  script: 'C:\\system\\mailer_jadlog\\bin\\www',
  nodeOptions: [
    '--harmony',
    '--max_old_space_size=8192'
  ]
  //, workingDirectory: '...'
  //, allowServiceLogon: true
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();