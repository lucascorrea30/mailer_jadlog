var express = require('express');
const { ObjectId } = require('mongodb');
var router = express.Router();
var reqApiJadlog = require('../services/request-api-jadlog');

/* GET home page. */
router.get('/', global.authenticationMiddleware(), function(req, res, next) {
    status_result = 0;
    msg = "";
    if (req.query.success !== undefined && req.query.success !== null && req.query.success == 1) {
        status_result = 1;
        msg = "Remessas enviadas com sucesso!";
    } else if (req.query.error !== undefined && req.query.error !== null) {
        status_result = 2;
        msg = "Erro ao enviar planilha";
    }
    res.render('pages/import', { page_name: "import", status_result: status_result, message: msg });

});

router.post('/', function(req, res, next) {
    var formidable = require('formidable');
    var fs = require('fs');
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        // if (err) throw err;
        if (files.remessaslist === undefined || files.remessaslist == null || files.remessaslist.size < 10) {
            res.redirect('/import?error=2');
        } else {

            var oldpath = files.remessaslist.path;
            var newpath = 'public/files/lista_remessa.xls';

            fs.readFile(oldpath, function(err, data) {
                if (err) throw err;
                console.log('File read!');

                // Write the file
                fs.writeFile(newpath, data, function(err) {
                    if (err) throw err;
                    console.log('File uploaded and moved!');
                    // res.end();
                    console.log('File written!');
                    readSheet(data, function() {
                        res.redirect('/import?success=1');
                    });

                });

                // Delete the file
                fs.unlink(oldpath, function(err) {
                    if (err) throw err;
                    console.log('File deleted!');
                });
            });
        }
    });
});

function readSheet(data, callback) {
    var XLSX = require('xlsx'); //, request = require('request');
    var workbook = XLSX.read(data, { type: 'buffer' });
    var sheet_name_list = workbook.SheetNames;
    var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

    // Percorre todas as linhas do excel buscando informação (da remessa)
    var lines = 0;
    xlData.forEach(element => {
        lines++;
        if (lines > 0) {
            let keys = Object.keys(element);

            if (element[keys[1]].length > 0) {
                let dataEvento = 0;
                if (element[keys[0]] != null) dataEvento = element[keys[0]];
                let remessa = 0;
                if (element[keys[1]] != null) remessa = element[keys[1]];
                let volume = 1;
                if (element[keys[24]] != null) volume = element[keys[24]];

                // var resClient = getClient(remessa, function(resp){
                //   if (resp.status && resp.status == true) {
                const texto = {
                    "remessa": remessa,
                    "local": "",
                    "motivo": "",
                    "medida": ""
                }
                const email = {
                    "data": dataEvento, //resp.tracking.dtEmissao,
                    "dataEvento": dataEvento,
                    "remessa": remessa,
                    "texto": texto,
                    "status_remessa": "", //resp.tracking.status,
                    "client": {}, //resp.client,
                    "sended": false,
                    "status": "IMPORT",
                    "nfe": "",
                    "unidade": "",
                    "dest": "",
                    "cepDest": "",
                    "prazo": "",
                    "volumes": { "qnt": volume, "isUnity": false, "isSended": false },
                    "sendClient": false,
                    "sendedUnity": false
                }
                db.collection("emails").insertOne(email, function(err, res) {
                    //console.log(email);
                    if (err) console.error(err);
                    else console.log(email.remessa + " - 1 email inserted");
                });
                //     }
                // });
            }
        }
    });

    callback();
}

function getClient(remessa, callback) {

    // remessa = 18134200561303;
    db.collection("clients").find({}).toArray(function(err, result) {
        if (err) throw err;
        result.forEach(function(client) {
            // console.log("\n---");
            // console.log("Nome: "+ client.name);
            // console.log("token: "+ client.token);

            getTracking(remessa, client.token, function(err, res, body) {
                var answer = { status: false, client: null };
                if (err) {
                    console.log(err);
                } else if (res.statusCode == 200 && body !== undefined) {
                    var resp = body.consulta[0];
                    if (resp.error) {
                        // console.log("Cliente "+client.name+":");
                        // console.log(resp.error);
                    } else {
                        // console.log("Cliente "+client.name+":");
                        // console.log(resp.tracking);
                        answer.status = true;
                        answer.client = client;
                        answer.tracking = resp.tracking;
                    }
                } else {
                    // console.log("StatusCode: "+res.statusCode);
                    // console.log("Response: "+body);
                }
                // console.log(answer);
                callback(answer);
            });
        });
    });
}

async function getTracking(cte, token, callback) {
    var data = { "consulta": [{ "cte": cte }] };
    var path = "/tracking/consultar";
    var type = "application/json";

    callback(await reqApiJadlog(data, path, token, type));
}

module.exports = router;