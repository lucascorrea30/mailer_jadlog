var express = require('express');
var router = express.Router();
const find = function (table, criteria) { 
  return db.collection(table).find(criteria).toArray();
}

/* GET home page. */
router.get('/', global.authenticationMiddleware(), function(req, res, next) {
	
   Promise.all(
         [find("clients", {}), find("emails", {'sended':true}), find("emails", {'sended':false,'status':{$exists:true,$nin:statusForbbidenClient}})]
   ).then(data => {
     res.render('pages/index', { page_name: "index", clients: data[0].length, emails: (data[1].length + data[2].length), emailsResolvidos: data[1].length, emailsEmAberto: data[2].length });
   }); 
});

router.get('/index', global.authenticationMiddleware(), function(req, res, next) {
  res.redirect('/');
});

module.exports = router;

const statusPending = [
  '',
  null,
  'AVARIA / DANO PARCIAL',
  'AVARIA / DANO TOTAL',
  'CEP ERRADO',
  'CORTE DE CARGA',
  'ENTREGA PARCIAL',
  'ERRO DE TRIAGEM / SEPARACAO',
  'ERRO DO EMISSOR',
  'EXTRAVIO PARCIAL',
  'EXTRAVIO TOTAL',
  'FURTO / ROUBO',
  'INDICIO VIOLACAO',
  'NAO ENTROU NA UNIDADE',
  'REITINERACAO - ERRO DE ENDERECO',
  'ENTREGA PARCIAL',
  'TERMO DE IRREGULARIDADE',
  'SOLICITACAO ENTREGA FUTURA',
  'FALHA ENTREGA',
  'SOLICITACAO ENTREGA FUTURA',
  'RETORNO DE PROTOCOLO',
  'SOLICITACAO DE ACAREACAO',
  'ATRASO TRANSPORTE',
  'RECUSADO - AVARIA',
  'CONTATE SEU FORNECEDOR',
  'OUTROS'
];

const statusForbbidenClient = statusPending;
statusForbbidenClient.push('EM ROTA');
statusForbbidenClient.push('EM DEVOLUCAO');
statusForbbidenClient.push('ENTRADA');
statusForbbidenClient.push('ENTREGUE');
statusForbbidenClient.push('EMISSAO');
statusForbbidenClient.push('TRANSFERENCIA');
statusForbbidenClient.push('UNITIZADO');
statusForbbidenClient.push('TRAVADO');
statusForbbidenClient.push('TRANSFERIDO PARA UNIDADE');
statusForbbidenClient.push('IMPORT');
