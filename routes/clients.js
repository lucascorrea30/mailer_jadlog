var express = require('express');
const { ObjectId } = require('mongodb');
var router = express.Router();

const formidableMiddleware = require('express-formidable'); 
router.use(formidableMiddleware());

/* GET home page. */
router.get('/', global.authenticationMiddleware(), function(req, res, next) {
  status_result = 0;
  msg = "";
  if (req.query.success !== undefined && req.query.success !== null && (req.query.success == 1 || req.query.success == 'true')) {
    status_result = 1;
    if (req.query.remove !== undefined && req.query.remove !== null && (req.query.remove == 1 || req.query.remove == 'true')) {
      msg = "Cliente removido com sucesso!";
    } else if (req.query.create !== undefined && req.query.create !== null && (req.query.create == 1 || req.query.create == 'true')) {
      msg = "Cliente cadastrado com sucesso!";
    } else {
      msg = "Clientes cadastrados com sucesso!";
    }
  } else if (req.query.error !== undefined && req.query.error !== null) {
    status_result = 2;
    if (req.query.error == 1) {
      msg = "Você precisa estar ciente e aceitar os termos de inserção descritos acima.";
    } else if (req.query.error == 2) {
      msg = "Planilha de inserção vazia ou inválida.";
    }
  }
  db.collection("clients").find({}).sort({ name: 1 }).toArray(function(err, result) {
    if (err) throw err;
    res.render('pages/clients', { page_name: "clients", clients: result, status_result: status_result, message: msg });
  });

});

router.post('/', function(req, res, next) {
  var formidable = require('formidable');
  var fs = require('fs');
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    // if (err) throw err;
    if (files.clientslist === undefined || files.clientslist == null || files.clientslist.size < 10) {
      res.redirect('/clients?error=2');
    } else {

      // Limpa tabela de clientes
      if (fields.chk_cleanAll !== undefined && fields.chk_cleanAll != null && fields.chk_cleanAll == 'on') {
        if (fields.chk_ack === undefined || fields.chk_ack == null || fields.chk_ack != 'on') {
          res.redirect('/clients?error=1');
          return;
        } else {
          db.collection("clients").remove({});
        }
      }

      var oldpath = files.clientslist.path;
      var newpath = 'public/files/lista_clientes.xls';
  
      fs.readFile(oldpath, function (err, data) {
        if (err) throw err;
        console.log('File read!');
  
        // Write the file
        fs.writeFile(newpath, data, function (err) {
            if (err) throw err;
            console.log('File uploaded and moved!');
            // res.end();
            console.log('File written!');
            readSheet(data);
  
            res.redirect('/clients?success=1');
        });
  
        // Delete the file
        fs.unlink(oldpath, function (err) {
            if (err) throw err;
            console.log('File deleted!');
        });
      });
    }
  });
});

/* CREATE */
router.get('/create', function(req, res){
  res.render('pages/clients/create', { page_name: "clients" });
});  
router.post('/create', async function(req, res){
  let name  = req.fields.name;
  let cnpj  = req.fields.cnpj;
  let email = req.fields.email;
  let token = req.fields.token;

  var newClient = { 
    name: name,
    cnpj: cnpj,
    email: email,
    token: token
  };
  await db.collection("clients").insertOne(newClient, async function(err, res) {
    if (err) {
      return console.error(err);
    }
    console.log("1 client inserted");
  });
  res.redirect('/clients?success=true&create=true');
});

/* EDIT */
router.get('/:id/edit', function(req, res){
  let id = req.params.id;
  let resp = {
    'success': false,
    'isset': false
  };

  if (req.query.success !== undefined) {
    resp.success = req.query.success;
    resp.isset = true;
  }

  db.collection('clients').find(ObjectId(id)).toArray((err, result) => {
    if (err) return res.send(err);
    res.render('pages/clients/edit', { page_name: "clients", client: result[0], is_update:resp });
  })
});

router.post('/:id/edit', function(req, res){
  let id = req.params.id;
  let input = req.fields;

  db.collection("clients").updateOne({_id:ObjectId(id)}, {
    $set: {
      name:input.name,
      cnpj:input.cnpj,
      token:input.token,
      email:input.email
    }
  }, function(err, res) {
    if (err) throw err;
    console.log("1 document updated");
  });

  res.redirect('/clients/'+ id +'/edit?success=true');
});

/* DELETE */
router.get('/:id/remove', function(req, res){
  let id = req.params.id;

  db.collection("clients").deleteOne({_id:ObjectId(id)}, function(err, obj) {
    if (err) throw err;
    console.log("1 document deleted");
    res.redirect('/clients?success=true&remove=true');
  });
});

/* EXPORTAR */
router.get('/exportar', function(req, res){

  db.collection("clients").find({}).sort({ name: 1 }).toArray(function(err, result) {
    if (err) throw err;
    res.render('pages/clients', { page_name: "clients", clients: result, status_result: status_result, message: msg });
  });

});

/* FUNCTIONS */
function readSheet(data) {
  var XLSX = require('xlsx'); //, request = require('request');
  var workbook = XLSX.read(data, {type:'buffer'});
  var sheet_name_list = workbook.SheetNames;
  var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

  // Percorre todas as linhas do excel buscando informação (que tiver token)
  var lines = 0;
  xlData.forEach(element => { 
    lines++;
    if (lines > 0) {
      var keys = Object.keys(element);
  
      if (element[keys[2]] !== undefined && element[keys[2]].length > 0) {
        var clientName = "";
        if (element[keys[0]] !== undefined && element[keys[0]] != null) clientName = element[keys[0]];
        var clientCnpj = "";
        if (element[keys[1]] !== undefined && element[keys[1]] != null) clientCnpj = element[keys[1]];
        var clientToken = "";
        if (element[keys[2]] !== undefined && element[keys[2]] != null) clientToken = element[keys[2]];
        var clientEmail = "";
        if (element[keys[3]] !== undefined && element[keys[3]] != null) clientEmail = element[keys[3]].replace(/;/g, ',');
  
        var newClient = { 
          name: clientName,
          cnpj: clientCnpj,
          token: clientToken,
          email: clientEmail,
        };
        db.collection("clients").findOne({"token":newClient.token}, function(err, result) {
          if (err) console.error(err);
          else {
            if (result) {
              db.collection("clients").updateOne({_id:ObjectId(result._id)}, {
                $set: {
                  name:newClient.name,
                  cnpj:newClient.cnpj,
                  token:newClient.token,
                  email:newClient.email
                }
              }, function(err, res) {
                if (err) throw err;
                console.log("1 document updated");
              });
            } else {
              db.collection("clients").insertOne(newClient, function(err, res) {
                if (err) throw err;
                console.log("1 document inserted");
              });
            }
          }
        });
      }
    }
  });
}

module.exports = router;
