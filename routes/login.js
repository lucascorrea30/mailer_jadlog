var passport = require('passport');

var express = require('express');
var router = express.Router();

// var bodyParser = require( 'body-parser' );
// router.use( bodyParser.urlencoded({ extended: true }) );

/* GET home page. */
router.get('/', function(req, res, next) {
  // verify(req, res);
  res.render('pages/login', { page_name: "login", message: null });
});

router.get('/login', function(req, res){
  console.log("Login GET");
  if(req.query.fail) {
    res.render('pages/login', { page_name: "login", message: 'Usuário e/ou senha incorretos!' });
  } else {
    res.redirect('/index');
  }
});

router.post('/', function(req, res) {
  verify(req, res);
});

function verify(req, res) {
  passport.authenticate('local', function (err, user, info) {
    if (err) { return next(err); }
    if (!user) { return res.redirect('/login?fail=true'); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return res.redirect('/index');
    });
  })(req, res);
}
module.exports = router;
