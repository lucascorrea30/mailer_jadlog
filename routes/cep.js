var express = require('express');
const { ObjectId } = require('mongodb');
var router = express.Router();

const formidableMiddleware = require('express-formidable'); 
router.use(formidableMiddleware());

/* GET home page. */
router.get('/', global.authenticationMiddleware(), function(req, res, next) {
  status_result = 0;
  msg = "";
  if (req.query.success !== undefined && req.query.success !== null && (req.query.success == 1 || req.query.success == 'true')) {
    status_result = 1;
    if (req.query.remove !== undefined && req.query.remove !== null && (req.query.remove == 1 || req.query.remove == 'true')) {
      msg = "CEP removido com sucesso!";
    } else if (req.query.create !== undefined && req.query.create !== null && (req.query.create == 1 || req.query.create == 'true')) {
      msg = "CEP cadastrado com sucesso!";
    } else {
      msg = "CEPs cadastrados com sucesso!";
    }
  } else if (req.query.error !== undefined && req.query.error !== null) {
    status_result = 2;
    if (req.query.error == 1) {
      msg = "Você precisa estar ciente e aceitar os termos de inserção descritos acima.";
    } else if (req.query.error == 2) {
      msg = "Planilha de inserção vazia ou inválida.";
    }
  }
  db.collection("cep").find({}).sort({ name: 1 }).toArray(function(err, result) {
    if (err) throw err;
    res.render('pages/cep', { page_name: "cep", cep: result, status_result: status_result, message: msg });
  });

});

router.post('/', function(req, res, next) {
  var formidable = require('formidable');
  var fs = require('fs');
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    // if (err) throw err;
    if (files.ceplist === undefined || files.ceplist == null || files.ceplist.size < 10) {
      res.redirect('/cep?error=2');
    } else {

      // Limpa tabela de clientes
      if (fields.chk_cleanAll !== undefined && fields.chk_cleanAll != null && fields.chk_cleanAll == 'on') {
        if (fields.chk_ack === undefined || fields.chk_ack == null || fields.chk_ack != 'on') {
          res.redirect('/cep?error=1');
          return;
        } else {
          db.collection("cep").remove({});
        }
      }

      var oldpath = files.ceplist.path;
      var newpath = 'public/files/lista_ceps.xls';
  
      fs.readFile(oldpath, function (err, data) {
        if (err) throw err;
        console.log('File read!');
  
        // Write the file
        fs.writeFile(newpath, data, function (err) {
            if (err) throw err;
            console.log('File uploaded and moved!');
            // res.end();
            console.log('File written!');
            readSheet(data);
  
            res.redirect('/cep?success=1');
        });
  
        // Delete the file
        fs.unlink(oldpath, function (err) {
            if (err) throw err;
            console.log('File deleted!');
        });
      });
    }
  });
});

/* CREATE */
router.get('/create', function(req, res){
  db.collection("unities").find({}).sort({ name: 1 }).toArray(function(err, result) {
    if (err) throw err;
    res.render('pages/cep/create', { page_name: "cep", unities: result });
  });
});  
router.post('/create', async function(req, res){
  let min  = req.fields.min;
  let max  = req.fields.max;
  let deadline = req.fields.deadline;
  let unity = req.fields.unity;
  let local = req.fields.local;
  let uf = req.fields.uf;

  var newClient = { 
    min: min,
    max: max,
    deadline: deadline,
    unity: unity,
    local: local,
    uf: uf
  };
  await db.collection("cep").insertOne(newClient, async function(err, res) {
    if (err) {
      return console.error(err);
    }
    console.log("1 client inserted");
  });
  res.redirect('/cep?success=true&create=true');
});

/* EDIT */
router.get('/:id/edit', function(req, res){
  let id = req.params.id;
  let resp = {
    'success': false,
    'isset': false
  };

  if (req.query.success !== undefined) {
    resp.success = req.query.success;
    resp.isset = true;
  }

  db.collection('cep').find(ObjectId(id)).toArray((err, result) => {
    if (err) return res.send(err);
    db.collection("unities").find({}).sort({ name: 1 }).toArray(function(err, resultUnities) {
      if (err) throw err;
      res.render('pages/cep/edit', { page_name: "cep", cep: result[0], unities: resultUnities, is_update:resp });
    });
  })
});

router.post('/:id/edit', function(req, res){
  let id = req.params.id;
  let input = req.fields;

  db.collection("cep").updateOne({_id:ObjectId(id)}, {
    $set: { 
      min: input.min,
      max: input.max,
      deadline: input.deadline,
      unity: input.unity,
      local: input.local,
      uf: input.uf
    }
  }, function(err, res) {
    if (err) throw err;
    console.log("1 document updated");
  });

  res.redirect('/cep/'+ id +'/edit?success=true');
});

/* DELETE */
router.get('/:id/remove', function(req, res){
  let id = req.params.id;

  db.collection("cep").deleteOne({_id:ObjectId(id)}, function(err, obj) {
    if (err) throw err;
    console.log("1 document deleted");
    res.redirect('/cep?success=true&remove=true');
  });
});

/* EXPORTAR */
router.get('/exportar', function(req, res){

  db.collection("cep").find({}).sort({ name: 1 }).toArray(function(err, result) {
    if (err) throw err;
    res.render('pages/cep', { page_name: "cep", ceps: result, status_result: status_result, message: msg });
  });

});

/* FUNCTIONS */
function readSheet(data) {
    var XLSX = require('xlsx'); //, request = require('request');
    var workbook = XLSX.read(data, {type:'buffer'});
    var sheet_name_list = workbook.SheetNames;
    var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

    // Percorre todas as linhas do excel buscando informação (que tiver token)
    var lines = 0;
    xlData.forEach(element => { 
        lines++;
        if (lines > 14) {
            var keys = Object.keys(element);

            if (element[keys[2]].length > 0) {
                var cepUf = 0;
                if (element[keys[0]] !== undefined && element[keys[0]] != null) cepUf = element[keys[0]];
                var cepLocal = 0;
                if (element[keys[1]] !== undefined && element[keys[1]] != null) cepLocal = element[keys[1]];
                var cepMin = 0;
                var cepMax = 0;
                if (element[keys[2]] !== undefined && element[keys[2]] != null) {
                    var aux = element[keys[2]].split(' a ');
                    cepMin = aux[0];
                    cepMax = aux[1];
                }
                var cepDeadline = 0;
                if (element[keys[4]] !== undefined && element[keys[4]] != null) cepDeadline = element[keys[4]];
                var cepUnity = "";
                if (element[keys[7]] !== undefined && element[keys[7]] != null) cepUnity = element[keys[7]];

                var newCep = { 
                uf: cepUf,
                local: cepLocal,
                min: cepMin,
                max: cepMax,
                deadline: cepDeadline,
                unity: cepUnity
                };
                db.collection("cep").findOne({$or:[{min:newCep.min},{max:newCep.max}]}, function(err, result) {
                    if (err) console.error(err);
                    else {
                        if (result) {
                            db.collection("cep").updateOne({_id:ObjectId(result._id)}, {
                                $set: {
                                uf:newCep.uf,
                                local:newCep.local,
                                min:newCep.min,
                                max:newCep.max,
                                deadline:newCep.deadline,
                                unity:newCep.unity
                                }
                            }, function(err, res) {
                                if (err) throw err;
                                console.log("1 document updated");
                            });
                        } else {
                            db.collection("cep").insertOne(newCep, function(err, res) {
                                if (err) throw err;
                                console.log("1 document inserted");
                            });
                        }
                    }
                });
            }
        }
  });
}

module.exports = router;
