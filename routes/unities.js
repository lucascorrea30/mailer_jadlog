var express = require('express');
const { ObjectId } = require('mongodb');
var router = express.Router();

const formidableMiddleware = require('express-formidable'); 
router.use(formidableMiddleware());

/* GET home page. */
router.get('/', global.authenticationMiddleware(), function(req, res, next) {
  status_result = 0;
  msg = "";
  if (req.query.success !== undefined && req.query.success !== null && (req.query.success == 1 || req.query.success == 'true')) {
    status_result = 1;
    if (req.query.remove !== undefined && req.query.remove !== null && (req.query.remove == 1 || req.query.remove == 'true')) {
      msg = "Unidade removida com sucesso!";
    } else if (req.query.create !== undefined && req.query.create !== null && (req.query.create == 1 || req.query.create == 'true')) {
      msg = "Unidade cadastrada com sucesso!";
    } else {
      msg = "Unidades cadastradas com sucesso!";
    }
  } else if (req.query.error !== undefined && req.query.error !== null) {
    status_result = 2;
    if (req.query.error == 1) {
      msg = "Você precisa estar ciente e aceitar os termos de inserção descritos acima.";
    } else if (req.query.error == 2) {
      msg = "Planilha de inserção vazia ou inválida.";
    }
  }
  db.collection("unities").find({}).sort({ name: 1 }).toArray(function(err, result) {
    if (err) throw err;
    res.render('pages/unities', { page_name: "unities", unities: result, status_result: status_result, message: msg });
  });

});

router.post('/', function(req, res, next) {
  var formidable = require('formidable');
  var fs = require('fs');
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    // if (err) throw err;
    if (files.unitieslist === undefined || files.unitieslist == null || files.unitieslist.size < 10) {
      res.redirect('/unities?error=2');
    } else {

      // Limpa tabela de unidades
      if (fields.chk_cleanAll !== undefined && fields.chk_cleanAll != null && fields.chk_cleanAll == 'on') {
        if (fields.chk_ack === undefined || fields.chk_ack == null || fields.chk_ack != 'on') {
          res.redirect('/unities?error=1');
          return;
        } else {
          db.collection("unities").remove({});
        }
      }

      var oldpath = files.unitieslist.path;
      var newpath = 'public/files/lista_unidades.xls';
  
      fs.readFile(oldpath, function (err, data) {
        if (err) throw err;
        console.log('File read!');
  
        // Write the file
        fs.writeFile(newpath, data, function (err) {
            if (err) throw err;
            console.log('File uploaded and moved!');
            // res.end();
            console.log('File written!');
            readSheet(data);
  
            res.redirect('/unities?success=1');
        });
  
        // Delete the file
        fs.unlink(oldpath, function (err) {
            if (err) throw err;
            console.log('File deleted!');
        });
      });
    }
  });
});


/* CREATE */
router.get('/create', function(req, res){
  res.render('pages/unities/create', { page_name: "unities" });
});  
router.post('/create', async function(req, res){
  let name  = req.fields.name;
  let cnpj  = req.fields.cnpj;
  let email = req.fields.email;

  var newUnity = { 
    name: name,
    cnpj: cnpj,
    email: email
  };
  await db.collection("unities").insertOne(newUnity, async function(err, res) {
    if (err) {
      return console.error(err);
    }
    console.log("1 unity inserted");
  });
  res.redirect('/unities?success=true&create=true');
});

/* EDIT */
router.get('/:id/edit', function(req, res){
  let id = req.params.id;
  let resp = {
    'success': false,
    'isset': false
  };

  if (req.query.success !== undefined) {
    resp.success = req.query.success;
    resp.isset = true;
  }

  db.collection('unities').find(ObjectId(id)).toArray((err, result) => {
    if (err) return res.send(err);
    res.render('pages/unities/edit', { page_name: "unities", unity: result[0], is_update:resp });
  })
});

router.post('/:id/edit', function(req, res){
  let id = req.params.id;
  let input = req.fields;

  db.collection("unities").updateOne({_id:ObjectId(id)}, {
    $set: {
      name:input.name,
      cnpj:input.cnpj,
      email:input.email,
    }
  }, function(err, res) {
    if (err) throw err;
    console.log("1 document updated");
  });

  res.redirect('/unities/'+ id +'/edit?success=true');
});

/* DELETE */
router.get('/:id/remove', function(req, res){
  let id = req.params.id;

  db.collection("unities").deleteOne({_id:ObjectId(id)}, function(err, obj) {
    if (err) throw err;
    console.log("1 document deleted");
    res.redirect('/unities?success=true&remove=true');
  });
});

/* EXPORTAR */
router.get('/exportar', function(req, res){

  db.collection("unities").find({}).sort({ name: 1 }).toArray(function(err, result) {
    if (err) throw err;
    res.render('pages/unities', { page_name: "unities", unities: result, status_result: status_result, message: msg });
  });

});

/* FUNCTIONS */
function readSheet(data) {
  var XLSX = require('xlsx'); //, request = require('request');
  var workbook = XLSX.read(data, {type:'buffer'});
  var sheet_name_list = workbook.SheetNames;
  var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

  // Percorre todas as linhas do excel buscando informação (que tiver email)
  var lines = 0;
  
  xlData.forEach(element => { 
    lines++;
    if (lines > 0) {
      var keys = Object.keys(element);
  
      if (element['Nome Unidade'] !== undefined && element['Nome Unidade'].length > 0) {
        var unityName = "";
        if (element['Nome Unidade'] !== undefined && element['Nome Unidade'] != null) unityName = element['Nome Unidade'];
        var unityCnpj = "";
        if (element['CNPJ'] !== undefined && element['CNPJ'] != null) unityCnpj = element['CNPJ'];
        var unityEmail = "";
        if (element['E-mails'] !== undefined && element['E-mails'] != null) unityEmail = element['E-mails'].replace(/;/g, ',').trim();

        var newUnity = { 
          name: unityName,
          cnpj: unityCnpj,
          email: unityEmail,
        };
        db.collection("unities").findOne({"name":newUnity.name}, function(err, result) {
          if (err) console.error(err);
          else {
            if (result) {
              db.collection("unities").updateOne({_id:ObjectId(result._id)}, {
                $set: {
                  name:newUnity.name,
                  cnpj:newUnity.cnpj,
                  email:newUnity.email
                }
              }, function(err, res) {
                if (err) throw err;
                // console.log("1 document updated");
              });
            } else {
              db.collection("unities").insertOne(newUnity, function(err, res) {
                if (err) throw err;
                // console.log("1 document inserted");
              });
            }
          }
        });
      }
    }
  });
}

module.exports = router;
