const passport = require('passport')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)

var schedule = require('node-schedule');
const ReadEmail = require('./services/read-email');
// const reqReadEmail = new ReadEmail('jadlog.mailer@usejadlog.com.br', 'BobDyl@n1941', 'imap.task.com.br', 993, true, true);
// const reqReadEmailCrc = new ReadEmail('all.crc@jadlog.com.br', 'Trocar123@', 'email-ssl.com.br', 993, true, true);
// const reqReadEmailBhz = new ReadEmail('cslogistica.bhz@jadlog.com.br', 'Bh@2020!', 'email-ssl.com.br', 993, true, true);
// const reqReadEmailSea = new ReadEmail('cslogistica.sea@jadlog.com.br', '!123@Mudar#', 'email-ssl.com.br', 993, true, true);

const SendEmail = require('./services/send-email');
const reqSendEmail = new SendEmail('jadlog.mailer@usejadlog.com.br', 'BobDyl@n1941', 'smtp.task.com.br', 25, false, false);
// const reqSendEmail = new SendEmail('all.crc@jadlog.com.br', 'Trocar123@', 'email-ssl.com.br', 465, true);
var reqApiJadlog = require('./services/request-api-jadlog');

var loggerMain = require('./logger').createLogger('Main');

var moment = require('moment-business-days');

moment.updateLocale('pt-BR', {
    workingWeekdays: [1, 2, 3, 4, 5] // 0: sunday 6: saturday
});

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

global.authenticationMiddleware = () => {
    return function(req, res, next) {
        if (req.isAuthenticated()) {
            return next()
        }
        res.redirect('/login?fail=true')
    }
};

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var loginRouter = require('./routes/login');
var logoutRouter = require('./routes/logout');
var clientsRouter = require('./routes/clients');
var unitiesRouter = require('./routes/unities');
var cepRouter = require('./routes/cep');
var importRouter = require('./routes/import');

var app = express();

//autenticação
require('./auth')(passport);
app.use(session({
    store: new MongoStore({
        url: process.env.MONGO_CONNECTION,
        db: global.db,
        ttl: 30 * 60 // = 30 minutos de sessão
    }),
    secret: process.env.MONGO_STORE_SECRET, //configure um segredo seu aqui
    resave: false,
    saveUninitialized: false
}))
app.use(passport.initialize());
app.use(passport.session());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/index/', indexRouter);
app.use('/login/', loginRouter);
app.use('/logout/', logoutRouter);
app.use('/users/', usersRouter);
app.use('/clients/', clientsRouter);
app.use('/unities/', unitiesRouter);
app.use('/cep/', cepRouter);
app.use('/import/', importRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('pages/error');
});

loggerMain.info("Created");

// SCHEDULE
// *    *    *    *    *    *
// ┬    ┬    ┬    ┬    ┬    ┬
// │    │    │    │    │    │
// │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
// │    │    │    │    └───── month (1 - 12)
// │    │    │    └────────── day of month (1 - 31)
// │    │    └─────────────── hour (0 - 23)
// │    └──────────────────── minute (0 - 59)
// └───────────────────────── second (0 - 59, OPTIONAL)

// Todo dia
var sjRemoveOldEmail = schedule.scheduleJob('15 23 * * *', function(fireDate) {
    loggerMain.info('sjRemoveOldEmail');
    var today = new Date()
    var cutDate = new Date();
    cutDate.setDate(today.getDate() - 60);
    // db.collection("emails").find({data:{$lt:cutDate}, sended:true}).toArray(function(err, data){
    //   if (data.length > 0) console.log(data);
    //   else console.log("No records to delete");
    // });
    loggerMain.info('Apagando e-mails anteriores a ' + cutDate.toISOString());
    db.collection("emails").deleteMany({ data: { $lt: trataData(cutDate.toISOString()) }, sended: true }, (err, collection) => {
        if (err) {
            // throw err;
            loggerMain.error(err);
        }
        loggerMain.info(collection.result.n + " Registros apagados com sucesso");
        console.log(collection.result.n + " Record(s) deleted successfully");
        // console.log(collection);
    });
});

// A cada 5min
var sjReadEmail = schedule.scheduleJob('0 */5 * * * *', function(fireDate) {
    loggerMain.info('sjReadEmail at ' + fireDate);
    // let reqReadEmail =
    new ReadEmail('jadlog.mailer@usejadlog.com.br', 'BobDyl@n1941', 'imap.task.com.br', 993, true, true).read();
    // reqReadEmail.read();
});

// A cada 30min
// var sjCheckEmail = schedule.scheduleJob('* */5 * * *', function(fireDate){
var sjCheckEmail = schedule.scheduleJob('11 */10 * * * *', async function(fireDate) {
    loggerMain.info('sjCheckEmail');
    process.setMaxListeners(0);

    // Get Email sem cliente
    let emailsNoClient = await db.collection("emails").find({ 'client': {}, 'sended': false, 'status_remessa': { $nin: ["SEM CLIENTE"] } }).sort({ dataEvento: 1 }).limit(100).toArray();
    if (emailsNoClient != null && emailsNoClient !== undefined && emailsNoClient.length > 0) {
        for (let email of emailsNoClient) {
            if (email.client == null || email.client === undefined || isEmptyObject(email.client)) {

                loggerMain.info("Buscando cliente");
                let resp = await getClient(email.remessa);
                if (resp.status && resp.status == true && resp.client != null && resp.tracking != null) {
                    let dataEvento = resp.tracking.dtEmissao;
                    if (resp.tracking.eventos.length > 0) {
                        dataEvento = trataData(resp.tracking.eventos[resp.tracking.eventos.length - 1].data);
                    }
                    db.collection("emails").updateOne({ _id: email._id }, {
                        $set: {
                            data: resp.tracking.dtEmissao,
                            dataEvento: dataEvento,
                            status_remessa: resp.tracking.status,
                            client: resp.client
                        }
                    });
                    loggerMain.info("Atualizando cliente");
                } else {
                    loggerMain.error("Erro ao atualizar cliente [remessa: " + email.remessa + "]");
                    loggerMain.error(resp);
                    console.log("Erro ao atualizar cliente [remessa: " + email.remessa + "]");
                    console.log(resp);

                    db.collection("emails").updateOne({ _id: email._id }, {
                        $set: {
                            status_remessa: "SEM CLIENTE",
                            sended: true
                        }
                    });
                }

            } else {
                loggerMain.error('sjCheckEmail: Remssa sem cliente');
                loggerMain.error(email);
            }
        }
    }

    let emails = await db.collection("emails").find({ 'status': { $nin: ['ENTREGUE'] }, 'sended': false }).sort({ dataEvento: 1 }).toArray();
    if (emails != null && emails !== undefined && emails.length > 0) {
        for (let email of emails) {
            if (email.client !== undefined && !isEmptyObject(email.client)) {

                let response = await getTracking(email.remessa, email.client.token);
                loggerMain.info('sjCheckEmail: getTracking');

                if (response !== undefined && response != null && response.consulta !== undefined) {

                    let resp = response.consulta[0];
                    if (resp.error) {
                        loggerMain.error('Error: ' + email.remessa + ' - Client: ' + email.client.name);
                        loggerMain.error(resp.error);
                    } else {

                        let status = email.status_remessa;
                        let unidade = "";
                        let volumes = 1;
                        // loggerMain.info('sjCheckEmail: Remessa: '+email.remessa);
                        if (resp.tracking.eventos.length > 0) {
                            dataEvento = trataData(resp.tracking.eventos[resp.tracking.eventos.length - 1].data);
                            status = resp.tracking.eventos[resp.tracking.eventos.length - 1].status;
                            unidade = resp.tracking.eventos[resp.tracking.eventos.length - 1].unidade;
                            volumes = resp.tracking.volumes.length;
                            // loggerMain.info('sjCheckEmail: Vol Length: '+resp.tracking.volumes.length);
                        }

                        switch (status) {
                            case 'DEVOLVIDO':
                            case 'ENTREGUE':
                                db.collection("emails").updateOne({ _id: email._id }, {
                                    $set: {
                                        status: status,
                                        sended: true
                                    }
                                });
                                break;
                            default:
                                let unityFinded = await db.collection("unities").findOne({ name: unidade });
                                if (unityFinded == null || unityFinded === undefined) {
                                    // loggerMain.error("sjCheckEmail: FindUnity Error");
                                    // loggerMain.error(unityFinded);
                                    // console.error("FindUnity Error");
                                    // console.error(unityFinded);
                                } else {
                                    var unityJson = {
                                            name: unidade,
                                            cpnj: "",
                                            email: ""
                                        }
                                        // if (data && data.length > 0 && data[0] != null) {
                                    unityJson = {
                                            _id: unityFinded._id,
                                            name: unityFinded.name,
                                            cpnj: unityFinded.cnpj,
                                            email: unityFinded.email
                                        }
                                        // }
                                    let statusEdited = (statusType[status] == null || statusType[status] === undefined) ? status : statusType[status];
                                    db.collection("emails").updateOne({ _id: email._id }, {
                                        $set: {
                                            sended: false,
                                            dataEvento: dataEvento,
                                            status: statusEdited,
                                            unidade: unityJson,
                                            "volumes.qnt": volumes
                                        }
                                    });
                                }

                                let trackingXml = await getXml(resp.tracking.dacte, email.client.token);
                                if (trackingXml != null && trackingXml !== undefined) {
                                    var parseString = require('xml2js').parseString;

                                    try {
                                        parseString(trackingXml.replace("\ufeff", ""), async function(err, result) {
                                            // console.log(result.cteProc.CTe[0].infCte[0].infCTeNorm[0].infDoc[0].infNFe);
                                            if (err) {
                                                loggerMain.error("sjCheckEmail: getXML");
                                                loggerMain.error(err);
                                                loggerMain.error(result);
                                                console.log("-----");
                                                console.log("Dacte: " + resp.tracking.dacte);
                                                console.log("Token: " + email.client.token);
                                                // console.log("Body: " + trackingXml);
                                                console.error(err);
                                                console.error(result);
                                                console.log("-----");
                                            } else if (result !== undefined && result.cteProc !== undefined && result.cteProc.CTe !== undefined && result.cteProc.CTe[0].infCte !== undefined) {
                                                if (
                                                    result.cteProc.CTe[0].infCte[0].infCTeNorm !== undefined &&
                                                    result.cteProc.CTe[0].infCte[0].infCTeNorm[0].infDoc !== undefined &&
                                                    result.cteProc.CTe[0].infCte[0].infCTeNorm[0].infDoc[0].infNFe !== undefined &&
                                                    result.cteProc.CTe[0].infCte[0].infCTeNorm[0].infDoc[0].infNFe[0].chave !== undefined
                                                ) {
                                                    db.collection("emails").updateOne({ _id: email._id }, {
                                                        $set: {
                                                            nfe: result.cteProc.CTe[0].infCte[0].infCTeNorm[0].infDoc[0].infNFe[0].chave[0]
                                                        }
                                                    });
                                                }
                                                if (
                                                    result.cteProc.CTe[0].infCte[0].dest !== undefined &&
                                                    (result.cteProc.CTe[0].infCte[0].dest[0].CPF !== undefined || result.cteProc.CTe[0].infCte[0].dest[0].CNPJ !== undefined) &&
                                                    result.cteProc.CTe[0].infCte[0].dest[0].xNome !== undefined
                                                ) {
                                                    var cpf_cnpj = (result.cteProc.CTe[0].infCte[0].dest[0].CPF !== undefined) ? result.cteProc.CTe[0].infCte[0].dest[0].CPF[0] : result.cteProc.CTe[0].infCte[0].dest[0].CNPJ[0];
                                                    db.collection("emails").updateOne({ _id: email._id }, {
                                                        $set: {
                                                            dest: result.cteProc.CTe[0].infCte[0].dest[0].xNome[0] + " " + cpf_cnpj
                                                        }
                                                    });
                                                }
                                                if (
                                                    result.cteProc.CTe[0].infCte[0].dest !== undefined &&
                                                    result.cteProc.CTe[0].infCte[0].dest[0].enderDest !== undefined &&
                                                    result.cteProc.CTe[0].infCte[0].dest[0].enderDest[0].CEP !== undefined &&
                                                    result.cteProc.CTe[0].infCte[0].ide !== undefined &&
                                                    result.cteProc.CTe[0].infCte[0].ide[0].dhEmi !== undefined
                                                ) {
                                                    let cep = result.cteProc.CTe[0].infCte[0].dest[0].enderDest[0].CEP[0];
                                                    let data = trataData(result.cteProc.CTe[0].infCte[0].ide[0].dhEmi[0]);
                                                    let ceps = await db.collection("cep").find({ "$and": [{ "min": { "$lte": cep } }, { "max": { "$gte": cep } }] }).toArray();
                                                    if (ceps != null && ceps !== undefined) {
                                                        for (let c of ceps) {
                                                            let prazo = trataData(moment(data, 'DD/MM/YYYY').businessAdd(c.deadline)._d.toISOString());
                                                            let isUnity = (email.unidade !== undefined && email.unidade.name == c.unity);
                                                            db.collection("emails").updateOne({ _id: email._id }, {
                                                                $set: {
                                                                    cepDest: cep,
                                                                    prazo: prazo,
                                                                    "volumes.isUnity": isUnity
                                                                }
                                                            });
                                                        }
                                                    } else {
                                                        loggerMain.error("sjCheckEmail: FindCEP Error");
                                                        loggerMain.error(ceps);
                                                    }
                                                }

                                            } else {
                                                loggerMain.error("sjCheckEmail: Sem NF ou NF inválida");
                                                console.log("Sem NF ou NF inválida");
                                            }
                                        });
                                    } catch (err) {
                                        loggerMain.error("sjCheckEmail: getXML: catch");
                                        loggerMain.error(err);
                                    }
                                }
                        }
                    }

                } else {
                    loggerMain.error("sjCheckEmail: Erro na consulta da API");
                    loggerMain.error("");
                    console.error("Erro na consulta da API");
                    console.error(response);
                }

                // } else {
                //     loggerMain.error('sjCheckEmail atualizando remessas');
                //     loggerMain.error(email);
                //     console.error(email);
            }
        }
    }

});

// A cada 30min
// var sjSendEmailClient = schedule.scheduleJob('*/5 * * * * *', function(fireDate){
var sjSendEmailClient = schedule.scheduleJob('25 05 */2 * * *', async function(fireDate) {

    loggerMain.info('sjSendEmailClient');
    let clients = await db.collection("clients").find({}).sort({ data: 1 }).toArray();
    if (clients != null && clients !== undefined && clients.length > 0) {
        for (var client of clients) {

            let { ObjectId } = require('mongodb'); // or ObjectID 

            let emails = await db.collection("emails").find({ 'client._id': ObjectId(client._id), $or: [{ sendedClient: { $exists: false } }, { sendedClient: false }], 'sended': false, 'status': { $exists: true, $nin: statusForbbidenClient } }).sort({ 'data': 1 }).toArray();
            loggerMain.info('sjSendEmailClient: [client: ' + client.name + ', qntEmail: ' + emails.length + ']');
            if (emails != null && emails !== undefined && emails.length > 0) {
                for (var email of emails) {

                    var emailDest = "";
                    if (email.dest !== undefined && email.dest != null) emailDest = email.dest.toString();
                    var emailStatus = "";
                    if (email.status !== undefined && email.status != null) emailStatus = email.status.toString();

                    var msg = 'Olá, ' + client.name + '<br><br>' +
                        'ATENÇÃO!' + '<br><br>' +
                        'Remessa <strong>' + email.remessa + '</strong> em custódia <br><br>' +
                        // 'Status: '+status+'\n\n'+
                        '<table border=1>' +
                        '<thead>' +
                        '<tr>' +
                        '<th>Data</th>' +
                        '<th>Remessa</th>' +
                        '<th>NFe</th>' +
                        '<th>Destinatário</th>' +
                        '<th>Status</th>' +
                        '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '<tr>' +
                        '<td>' + email.data + '</td>' +
                        '<td>' + email.remessa + '</td>' +
                        '<td>' + email.nfe.substring(25, 34) + '</td>' +
                        '<td>' + emailDest + '</td>' +
                        '<td>' + emailStatus + '</td>' +
                        '</tr>' +
                        '</tbody>' +
                        '</table><br><br>' +
                        'Gentileza acessar o portal do cliente (https://www.jadlog.com.br/portalcliente/pages/login.jad) e realizar tratativas.' + '<br><br>' + '<br><br>' +
                        '<strong>E-mail automático. Favor <u>não</u> responder.</strong><br>' +
                        'Atenciosamente, <br>Equipe Jadlog';
                    // '\n\n\nE-mail: '+client.email;

                    let today = new Date();
                    // Salvo o envio do dia
                    let emailAlreadySendedToday = await db.collection("emails_sended").find({ 'email_id': email.remessa, 'email_status': emailStatus, 'send_date': today.toISOString().substring(0, 10) }).toArray();

                    if (emailAlreadySendedToday == null || emailAlreadySendedToday === undefined || emailAlreadySendedToday.length <= 0) {
                        db.collection("emails_sended").insertOne({
                            email_id: email.remessa,
                            email_status: emailStatus,
                            send_date: today.toISOString().substring(0, 10)
                        });

                        // Envio o Email
                        const from = 'jadlog.mailer@usejadlog.com.br';
                        const to = client.email;
                        const to_teste = 'lucas.nhl@gmail.com';
                        const subject = email.remessa + ' Pendências de Entrega Jadlog - ' + client.cnpj;
                        loggerMain.info('sjSendEmailClient: Remessa ' + email.remessa + ' enviada para ' + to + ', pendencias@usejadlog.com.br');
                        reqSendEmail.send(from, to + ', pendencias@usejadlog.com.br', subject, msg, {}, function() {
                            console.log(client.name);
                            console.log("Email enviado. To: " + to);

                            loggerMain.info('sjSendEmailClient: [client: ' + client.name + ', Email to: ' + to + ']');

                            db.collection("emails").update({ _id: email._id }, {
                                $set: {
                                    sendedClient: true,
                                    sendedClientDate: today.toISOString().substring(0, 10),
                                    sendedUnity: false
                                }
                            });
                        });

                    } else {
                        console.log(client.name);
                        console.log("Email ja enviado para: " + to);
                        loggerMain.info('sjSendEmailClient: [client: ' + client.name + ', Email ja enviado.]');
                    }
                }
            }

        }
    }

}); // sjSendEmailClient

// As 7h20min todo dia
// var sjSendEmailJadLog = schedule.scheduleJob('*/5 * * * * *', function(fireDate){
var sjSendEmailJadLog = schedule.scheduleJob('00 20 07 * * *', async function(fireDate) {

    loggerMain.info('sjSendEmailJadLog');
    let attendants = await db.collection("attendants").find({}).sort({ name: 1 }).toArray();
    if (attendants != null && attendants !== undefined && attendants.length > 0) {
        for (var attendant of attendants) {

            const { ObjectId } = require('mongodb'); // or ObjectID 

            let name = attendant.name.split(' ');
            const filename = 'pendencias_' + name[0] + '_' + attendant._id + '.xlsx';
            const fullFilename = './public/files/pendencias/' + filename;

            let emails = await db.collection("emails").find({ 'client._id': { $in: attendant.clients }, 'sended': false, 'status': { $exists: true, $in: statusPending } }).sort({ 'client.name': 1, 'data': 1 }).toArray();
            if (emails != null && emails !== undefined && emails.length > 0) {

                // Add Worksheets to the workbook
                let ws = wb.addWorksheet('Remessas');

                ws.cell(1, 1).string('Data Emissao').style(styleTitle);
                ws.cell(1, 2).string('Data Evento').style(styleTitle);
                ws.cell(1, 3).string('Remessa').style(styleTitle);
                ws.cell(1, 4).string('NFe').style(styleTitle);
                ws.cell(1, 5).string('Status').style(styleTitle);
                ws.cell(1, 6).string('Cliente').style(styleTitle);
                ws.cell(1, 7).string('Destinatário').style(styleTitle);

                let count = 0;

                for (let email of emails) {
                    count++;
                    let emailDest = "";
                    if (email.dest !== undefined && email.dest != null) emailDest = email.dest.toString();
                    let emailStatus = "";
                    if (email.status !== undefined && email.status != null) emailStatus = email.status.toString();
                    ws.cell(count + 1, 1).string(email.data).style(styleText);
                    ws.cell(count + 1, 2).string(email.dataEvento).style(styleText);
                    ws.cell(count + 1, 3).string(email.remessa).style(styleText);
                    ws.cell(count + 1, 4).string(email.nfe.substring(25, 34)).style(styleText);
                    ws.cell(count + 1, 5).string(emailStatus).style(styleText);
                    ws.cell(count + 1, 6).string(email.client.name).style(styleText);
                    ws.cell(count + 1, 7).string(emailDest).style(styleText);
                }

                if (count > 0) {
                    wb.write(fullFilename);

                    let msg = 'Olá, ' + attendant.name + '<br><br>' +
                        'ATENÇÃO!' + '<br><br>' +
                        'Remessa em custódia <br>' +
                        // 'Status: '+status+'\n\n'+
                        'Atenciosamente, <br>Equipe Jadlog';
                    // '\n\n\nE-mail: '+attendant.email;

                    let from = 'jadlog.mailer@usejadlog.com.br';
                    let to = attendant.email;
                    let to_teste = 'lucas.nhl@gmail.com';
                    // let to_teste = 'supervisao.sac@usejadlog.com.br';
                    let subject = 'Pendências de Entrega Jadlog';
                    reqSendEmail.send(from, to, subject, msg, { filename: filename, path: fullFilename, contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }, function() {
                        // db.collection("emails").update({_id:email._id}, {$set : {sended:true}})
                        // console.log(attendant.name);
                        console.log("Email enviado. To: " + to);
                    });
                    loggerMain.info('sjSendEmailJadLog: Enviado para ' + to);
                } else {
                    loggerMain.warn('sjSendEmailJadLog: Sem envio para atendente ' + attendant.name);
                }

            }
        }
    }

    // Envio da planilha de conferencia
    let yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 3);
    const filename = 'remessas_' + yesterday.toISOString().substring(0, 10) + '.xlsx';
    const fullFilename = './public/files/remessas_clients/' + filename;

    // Add Worksheets to the workbook
    let ws = wb.addWorksheet('Remessas');

    ws.cell(1, 1).string('Data').style(styleTitle);
    ws.cell(1, 2).string('Cliente').style(styleTitle);
    ws.cell(1, 3).string('Remessa').style(styleTitle);
    ws.cell(1, 4).string('NFe').style(styleTitle);
    ws.cell(1, 5).string('Destinatário').style(styleTitle);
    ws.cell(1, 6).string('Status').style(styleTitle);

    let emails = await db.collection("emails").find({ sendedClient: true, sendedClientDate: yesterday.toISOString().substring(0, 10), 'sended': false, 'status': { $exists: true, $nin: statusForbbidenClient } }).sort({ 'data': 1 }).toArray();
    if (emails != null && emails !== undefined && emails.length > 0) {

        let count = 0;
        for (let email of emails) {
            let emailDest = "";
            if (email.dest !== undefined && email.dest != null) emailDest = email.dest.toString();
            let emailStatus = "";
            if (email.status !== undefined && email.status != null) emailStatus = email.status.toString();

            count++;
            ws.cell(count + 1, 1).string(email.data).style(styleText);
            ws.cell(count + 1, 2).string(email.client.name).style(styleText);
            ws.cell(count + 1, 3).string(email.remessa).style(styleText);
            ws.cell(count + 1, 4).string(email.nfe.substring(25, 34)).style(styleText);
            ws.cell(count + 1, 5).string(emailDest).style(styleText);
            ws.cell(count + 1, 6).string(emailStatus).style(styleText);
        }

        if (count > 0) {
            wb.write(fullFilename);

            let msg = 'Olá, <br><br>' +
                'ATENÇÃO!' + '<br><br>' +
                'Segue em anexo as remessa em custódia enviadas aos clientes ontem<br>' +
                'Atenciosamente, <br>Equipe Jadlog';

            let from = 'jadlog.mailer@usejadlog.com.br';
            let to = 'supervisao.sac@usejadlog.com.br';
            let to_teste = 'lucas.nhl@gmail.com';
            let subject = 'Pendências de Entrega Jadlog - Geral';
            reqSendEmail.send(from, to + ', pendencias@usejadlog.com.br', subject, msg, { filename: filename, path: fullFilename, contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" }, function() {
                console.log("Email enviado. To: Pendencias");
            });
            loggerMain.info('sjSendEmailJadLog: E-mail de conferencia enviado para ' + to + ', pendencias@usejadlog.com.br');
        } else {
            loggerMain.warn('sjSendEmailJadLog: Sem envio de conferencia');
        }

    }

}); // sjSendEmailJadlog

// As 7h00 todo dia
// var sjSendEmailUnidade = schedule.scheduleJob('*/5 * * * * *', function(fireDate){
var sjSendEmailUnidade = schedule.scheduleJob('00 00 07 * * *', async function(fireDate) {

    let hoje = trataData(new Date().toISOString());
    loggerMain.info('sjSendEmailUnidade: ' + hoje);

    let unities = await db.collection("unities").find({ email: { $nin: ["", null] } }).sort({ data: 1 }).toArray();
    if (unities != null && unities !== undefined && unities.length > 0) {
        for (var unity of unities) {

            let { ObjectId } = require('mongodb'); // or ObjectID 

            // Remessas em Atraso
            let statusForbidden = statusPending.concat(Object.keys(statusType));
            statusForbidden.push('IMPORT');
            let emailsLate = await db.collection("emails").find({ 'unidade._id': ObjectId(unity._id), sended: false, prazo: { $lt: hoje }, status: { $nin: statusForbidden } }).toArray();
            if (emailsLate != null && emailsLate !== undefined && emailsLate.length > 0) {

                // Require library
                var responseTable = "<table border=1>" +
                    "<thead>" +
                    "<tr>" +
                    "<th>Data</th>" +
                    "<th>Prazo</th>" +
                    "<th>Remessa</th>" +
                    "<th>NFe</th>" +
                    "<th>Cliente</th>" +
                    "<th>Destinatário</th>" +
                    "<th>Status</th>" +
                    "</tr>" +
                    "</thead>" +
                    "<tbody>";

                var count = 0;

                for (var email of emailsLate) {
                    count++;
                    var emailClient = "";
                    if (email.client !== undefined && email.client != null) emailClient = email.client.name;
                    var emailDest = "";
                    if (email.dest !== undefined && email.dest != null) emailDest = email.dest.toString();
                    var emailStatus = "";
                    if (email.status !== undefined && email.status != null) emailStatus = email.status.toString();
                    responseTable += "<tr>" +
                        "<td>" + email.data + "</td>" +
                        "<td>" + email.prazo + "</td>" +
                        "<td>" + email.remessa + "</td>" +
                        "<td>" + email.nfe.substring(25, 34) + "</td>" +
                        "<td>" + emailClient + "</td>" +
                        "<td>" + emailDest + "</td>" +
                        "<td>" + emailStatus + "</td>" +
                        "</tr>";
                }


                if (count > 0) {
                    responseTable += "</tbody></table>";

                    var msg = 'Olá, ' + unity.name + '<br><br>' +
                        'ATENÇÃO!' + '<br><br>' +
                        'Remessas em atraso <br><br>' +
                        responseTable + '<br><br>' +
                        'Gentileza verificar e finalizar as entregas.' + '<br><br>' + '<br><br>' +
                        'Atenciosamente, <br>Equipe Jadlog' +
                        '<br><br><br>E-mail: ' + unity.email;

                    const from = 'jadlog.mailer@usejadlog.com.br';
                    const to = unity.email;
                    const to2 = 'supervisao.sac@usejadlog.com.br';
                    const to_teste = 'lucas.nhl@gmail.com';
                    const subject = 'Atrasos na Entrega Jadlog';
                    reqSendEmail.send(from, to + ', pendencias@usejadlog.com.br', subject, msg, null, function() {
                        // db.collection("emails").update({_id:email._id}, {$set : {sended:true}})
                        console.log("Atraso - Email enviado. To: " + to);
                    });
                    loggerMain.info('sjSendEmailUnidade: Cobranca de atraso enviado para ' + to + ', pendencias@usejadlog.com.br');
                    // console.log(msg);
                } else {
                    loggerMain.warn('sjSendEmailUnidade: Sem cobranca de atraso para ' + unity.name);
                }

            } // Fim Remessa em Atraso

            // Ação do Cliente
            let daysAgo = new Date();
            daysAgo.setDate(daysAgo.getDate() - 3);
            let emailsClientAction = await db.collection("emails").find({ 'unidade._id': ObjectId(unity._id), sended: false, $and: [{ sendedClientDate: daysAgo.toISOString().substring(0, 10) }, { sendedClient: true }, { sendedUnity: false }], status: { $nin: statusForbidden } }).toArray();
            if (emailsClientAction != null && emailsClientAction !== undefined && emailsClientAction.length > 0) {

                // Require library
                var responseTable = "<table border=1>" +
                    "<thead>" +
                    "<tr>" +
                    "<th>Data</th>" +
                    "<th>Prazo</th>" +
                    "<th>Remessa</th>" +
                    "<th>NFe</th>" +
                    "<th>Cliente</th>" +
                    "<th>Destinatário</th>" +
                    "<th>Status</th>" +
                    "</tr>" +
                    "</thead>" +
                    "<tbody>";

                let count = 0;
                let emailIds = [];

                for (var email of emailsClientAction) {
                    emailIds.push(email._id);
                    count++;
                    var emailClient = "";
                    if (email.client !== undefined && email.client != null) emailClient = email.client.name;
                    var emailDest = "";
                    if (email.dest !== undefined && email.dest != null) emailDest = email.dest.toString();
                    var emailStatus = "";
                    if (email.status !== undefined && email.status != null) emailStatus = email.status.toString();
                    responseTable += "<tr>" +
                        "<td>" + email.data + "</td>" +
                        "<td>" + email.prazo + "</td>" +
                        "<td>" + email.remessa + "</td>" +
                        "<td>" + email.nfe.substring(25, 34) + "</td>" +
                        "<td>" + emailClient + "</td>" +
                        "<td>" + emailDest + "</td>" +
                        "<td>" + emailStatus + "</td>" +
                        "</tr>";
                }

                if (count > 0) {
                    responseTable += "</tbody></table>";

                    var msg = 'Olá, ' + unity.name + '<br><br>' +
                        'ATENÇÃO!' + '<br><br>' +
                        'Conferir remessas abaixo <br><br>' +
                        responseTable + '<br><br>' +
                        'Gentileza verificar e finalizar as entregas.' + '<br><br>' + '<br><br>' +
                        'Atenciosamente, <br>Equipe Jadlog<br><br>';
                    // '<br><br><br>E-mail: '+unity.email;

                    const from = 'jadlog.mailer@usejadlog.com.br';
                    const to = unity.email;
                    const to2 = 'supervisao.sac@usejadlog.com.br';
                    const to_teste = 'lucas.nhl@gmail.com';
                    const subject = 'Ação do cliente na Entrega Jadlog';
                    reqSendEmail.send(from, to + ', pendencias@usejadlog.com.br', subject, msg, null, function() {
                        db.collection("emails").update({ _id: { $in: emailIds } }, { $set: { sendedUnity: true } })
                        console.log("Acao do cliente - Email enviado. To: " + to);
                    });

                    loggerMain.info('sjSendEmailUnidade: Alerta de acao do cliente enviado para ' + to + ', pendencias@usejadlog.com.br');
                    // console.log(msg);
                } else {
                    loggerMain.warn('sjSendEmailUnidade: Sem alerta de acao do cliente para ' + unity.name);
                }

            } // Fim Ação do Cliente

            // Entrega Parcial
            let emailsDelivery = await db.collection("emails").find({ 'unidade._id': ObjectId(unity._id), sended: false, "volumes.qnt": { $gt: 1 }, "volumes.isUnity": true, "volumes.isSended": false }).toArray();
            if (emailsDelivery != null && emailsDelivery !== undefined && emailsDelivery.length > 0) {

                // Require library
                var responseTable = "<table border=1>" +
                    "<thead>" +
                    "<tr>" +
                    "<th>Data</th>" +
                    "<th>Remessa</th>" +
                    "<th>NFe</th>" +
                    "<th>Cliente</th>" +
                    "<th>Destinatário</th>" +
                    "<th>Status</th>" +
                    "<th>Volumes</th>" +
                    "</tr>" +
                    "</thead>" +
                    "<tbody>";

                let count = 0;
                let emailIds = [];

                for (var email of emailsDelivery) {
                    count++;
                    emailIds.push(email._id);
                    var emailClient = "";
                    if (email.client !== undefined && email.client != null) emailClient = email.client.name;
                    var emailDest = "";
                    if (email.dest !== undefined && email.dest != null) emailDest = email.dest.toString();
                    var emailStatus = "";
                    if (email.status !== undefined && email.status != null) emailStatus = email.status.toString();
                    responseTable += "<tr>" +
                        "<td>" + email.data + "</td>" +
                        "<td>" + email.remessa + "</td>" +
                        "<td>" + email.nfe.substring(25, 34) + "</td>" +
                        "<td>" + emailClient + "</td>" +
                        "<td>" + emailDest + "</td>" +
                        "<td>" + emailStatus + "</td>" +
                        "<td>" + email.volumes.qnt + "</td>" +
                        "</tr>";
                }


                if (count > 0) {
                    responseTable += "</tbody></table>";

                    var msg = 'Olá, ' + unity.name + '<br><br>' +
                        'ATENÇÃO!' + '<br><br>' +
                        'Remessas abaixo foram emitidas com mais de um volume.' + '<br><br>' +
                        responseTable + '<br><br>' +
                        '<big><strong>Gentileza atentar e <span style=\"color:red\">NÃO</span> realizar entrega parcial</strong></big>' + '<br><br>' + '<br><br>' +
                        'Atenciosamente, <br>Equipe Jadlog';
                    // '<br><br><br>E-mail: '+unity.email;

                    const from = 'jadlog.mailer@usejadlog.com.br';
                    const to = unity.email;
                    const to2 = 'supervisao.sac@usejadlog.com.br';
                    const to_teste = 'lucas.nhl@gmail.com';
                    const subject = 'Entrega Parcial Jadlog';
                    reqSendEmail.send(from, to + ', pendencias@usejadlog.com.br', subject, msg, null, function() {
                        // db.collection("emails").update({_id:email._id}, {$set : {sended:true}})
                        console.log("parcial - Email enviado. To: " + to);
                        db.collection("emails").updateMany({ _id: { $in: emailIds } }, { $set: { "volumes.isSended": true } });
                    });

                    loggerMain.info('sjSendEmailUnidade: Alerta de entrega parcial enviado para ' + to + ', pendencias@usejadlog.com.br');
                    // console.log(msg);
                } else {
                    loggerMain.warn('sjSendEmailUnidade: Sem alerta de entrega parcial para ' + unity.name);
                }

            } // Fim Entrega Parcial

        }
    }

});


module.exports = app;


// Functions

async function getClient(remessa) {
    var answer = { status: false, client: null, tracking: null };

    let clients = await db.collection("clients").find({}).toArray();
    if (clients != null && clients !== undefined) {
        for (let client of clients) {

            let response = await getTracking(remessa, client.token);
            if (response !== undefined && response != null && response.consulta !== undefined) {
                let resp = response.consulta[0];
                if (resp.error) {
                    // console.log("Cliente "+client.name+":");
                    // console.log(resp.error);
                    // loggerMain.error(resp.error);
                } else {
                    answer.status = true;
                    answer.client = client;
                    answer.tracking = resp.tracking;

                    return answer;
                }
            }

        }
    }

    return answer;

}

async function getTracking(cte, token) {
    var data = { "consulta": [{ "cte": cte }] };
    var path = "/tracking/consultar";
    var type = "application/json";

    let resp = await reqApiJadlog(data, path, token, type);

    return resp;
}

async function getXml(dacte, token) {
    var data = { "dacte": dacte };
    var path = "/cte/xml";
    var type = "application/xml";

    return await reqApiJadlog(data, path, token, type);
}

function trataData(data) {
    if (data !== undefined && data.length > 1) {
        return data.substring(8, 10) + '/' + data.substring(5, 7) + '/' + data.substring(0, 4);
    } else {
        return data;
    }
}

function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}

const statusType = {
    'APREENSAO FISCAL DE DOCUMENTO E/OU MERCADORIA': 'Remessa retida. Gentileza enviar guia e comprovante de pagamento',
    'AUSENTE': 'Destinatário Ausente',
    'AUSENTE 1': 'Destinatário Ausente',
    'AUSENTE 2': 'Destinatário Ausente 2',
    'AUSENTE 3': 'Destinatário Ausente 3',
    'BUSCA': 'Busca Solicitada. Remessa com possível extravio, prazo para finalização são de 5 dias',
    'CAIXA POSTAL': 'Caixa Postal. Gentileza informar endereço alternativo, ou que o cliente faça retirada do produto na base.',
    'DESTINATARIO DESCONHECIDO': 'Destinatário desconhecido. Gentileza confirmar responsável pelo recebimento e telefone de contato',
    'DESTINATARIO SOLICITOU RETIRAR NA UNIDADE': 'Destinatário solicitou retirar na base',
    'DEVOLUCAO POR INSTRUCAO MATRIZ': 'Pedido em devolução',
    'DEVOLUCAO POR INSTRUCAO REMETENTE': 'Pedido em devolução',
    'ENDERECO EM ZONA RURAL': 'Endereço em zona rural. Gentileza informar endereço alternativo em perimetro urbano, ou solicitar que o cliente faça retirada na base.',
    'ENDERECO INSUFICIENTE': 'Endereço Insuficiente. Gentileza confirmar todos os dados completos (Rua, nº, Apto, Bloco, bairro)',
    'ENDERECO NAO LOCALIZADO': 'Endereço não localizado. Gentileza confirmar endereço de entrega , pontos de referência e contato ativo',
    'FECHADO': 'Local fechado.',
    'FECHADO 2': 'Local fechado 2.',
    'PARADO NA FISCALIZACAO': 'Remessa parada na fiscalização. Gentileza enviar guia e comprovante de pagamento.',
    'RESTRICAO DE ACESSO / MOVIMENTACAO': 'Área de Risco. Gentileza informar endereço alternativo, ou solicitar que o cliente faça retirada na base',
    'RETENCAO FISCAL DE DOCUMENTO E/OU MERCADORIA': 'Remessa em retenção fiscal. Gentileza enviar guia e comprovante de pagamento',
    'SAIDA FISCALIZACAO': 'Remessa liberada da retenção fiscal',
    'TELEFONE ERRADO': 'Telefone incorreto. Gentileza informar contato ativo',
    'TEMPO DE ESPERA EXCEDIDO NO DESTINATARIO': 'Tempo de espera excedido no destinatário. Gentileza informar destinatário que o tempo total de espera é de 10 minutos.',
    'TEMPORAL': 'Impossibilidade de entrega devido temporal.',
    'TRAFEGO INTERROMPIDO': 'Trafego Interrompido. Gentileza verificar novo endereço para entrega ou que o cliente faça retirada na base.',
    'FALTA NF': 'Gentileza enviar cópia da NF para o sac Jadlog',
};
// Atualizar no arquivo routes/index.js
const statusPending = [
    '',
    null,
    'AVARIA / DANO PARCIAL',
    'AVARIA / DANO TOTAL',
    'CEP ERRADO',
    'CORTE DE CARGA',
    'ENTREGA PARCIAL',
    'ERRO DE TRIAGEM / SEPARACAO',
    'ERRO DO EMISSOR',
    'EXTRAVIO PARCIAL',
    'EXTRAVIO TOTAL',
    'FURTO / ROUBO',
    'INDICIO VIOLACAO',
    'NAO ENTROU NA UNIDADE',
    'REITINERACAO - ERRO DE ENDERECO',
    'ENTREGA PARCIAL',
    'TERMO DE IRREGULARIDADE',
    'SOLICITACAO ENTREGA FUTURA',
    'FALHA ENTREGA',
    'SOLICITACAO ENTREGA FUTURA',
    'RETORNO DE PROTOCOLO',
    'SOLICITACAO DE ACAREACAO',
    'ATRASO TRANSPORTE',
    'RECUSADO - AVARIA',
    'CONTATE SEU FORNECEDOR',
    'OUTROS'
];

// Atualizar no arquivo routes/index.js
const statusForbbidenClient = statusPending;
statusForbbidenClient.push('EM ROTA');
statusForbbidenClient.push('EM DEVOLUCAO');
statusForbbidenClient.push('ENTRADA');
statusForbbidenClient.push('ENTREGUE');
statusForbbidenClient.push('EMISSAO');
statusForbbidenClient.push('TRANSFERENCIA');
statusForbbidenClient.push('UNITIZADO');
statusForbbidenClient.push('TRAVADO');
statusForbbidenClient.push('TRANSFERIDO PARA UNIDADE');
statusForbbidenClient.push('IMPORT');


// Require library
const xl = require('excel4node');

// Create a new instance of a Workbook class
const wb = new xl.Workbook();

// Create a reusable style
const styleTitle = wb.createStyle({
    font: {
        color: '#000000',
        size: 12,
        bold: true
    },
    fill: {
        type: 'pattern',
        bgColor: '#bfbfbf'
    },
    border: {
        left: {
            style: 'medium',
            color: '#000000'
        },
        right: {
            style: 'medium',
            color: '#000000'
        },
        top: {
            style: 'medium',
            color: '#000000'
        },
        bottom: {
            style: 'medium',
            color: '#000000'
        },
    },
    numberFormat: 'text',
});
const styleText = wb.createStyle({
    font: {
        color: '#000000',
        size: 12
    },
    border: {
        left: {
            style: 'thin',
            color: '#000000'
        },
        right: {
            style: 'thin',
            color: '#000000'
        },
        top: {
            style: 'thin',
            color: '#000000'
        },
        bottom: {
            style: 'thin',
            color: '#000000'
        },
    },
    numberFormat: 'text',
});